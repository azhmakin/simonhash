// Constructing a hash function from a block cipher
// Implementation by Andrey Zhmakin
// Simon implementation derived from: SIMON and SPECK Implementation Guide by NSA, 2019
// Miyaguchi-Preneel construction from: Understanding Cryptography by Christof Paar, Jan Pelzl, 2010, p.306

#include <iostream>
#include <iomanip>
#include <intrin.h>


void BytesToWords64(uint8_t bytes[], uint64_t words[], int numbytes)
{
    int i, j = 0;

    for (i = 0; i < numbytes / 8; i++) {
        words[i] = (uint64_t) bytes[j]
            | ((uint64_t)bytes[j + 1] << 8)
            | ((uint64_t)bytes[j + 2] << 16)
            | ((uint64_t)bytes[j + 3] << 24)
            | ((uint64_t)bytes[j + 4] << 32)
            | ((uint64_t)bytes[j + 5] << 40)
            | ((uint64_t)bytes[j + 6] << 48)
            | ((uint64_t)bytes[j + 7] << 56);
        j += 8;
    }
}


#define ROTL64(x,r) (((x) << (r)) | ((x) >> (64 - (r))))
#define ROTR64(x,r) (((x) >> (r)) | ((x) << (64 - (r))))

#define f64(x) ((ROTL64(x,1) & ROTL64(x,8)) ^ ROTL64(x,2))
#define R64x2(x,y,k1,k2) (y^=f64(x), y^=k1, x^=f64(y), x^=k2)


/**
 * \param K Two 64-bit words.
 * \paran rk Array of 68 64-bit elements.
 */
void Simon_128_128_KeySchedule(uint64_t K[], uint64_t rk[])
{
    uint64_t B = K[1], A = K[0];
    uint64_t c = UINT64_C(0xFFFFFFFFFFFFFFFC), z = UINT64_C(0x7369F885192C0EF5);

    for (int i = 0; i < 64;)
    {
        rk[i++] = A; A ^= c ^ (z & 1) ^ ROTR64(B, 3) ^ ROTR64(B, 4); z >>= 1;
        rk[i++] = B; B ^= c ^ (z & 1) ^ ROTR64(A, 3) ^ ROTR64(A, 4); z >>= 1;
    }

    rk[64] = A; A ^= c ^ 1 ^ ROTR64(B, 3) ^ ROTR64(B, 4);
    rk[65] = B; B ^= c ^ 0 ^ ROTR64(A, 3) ^ ROTR64(A, 4);
    rk[66] = A;
    rk[67] = B;
}


/**
 * \param Pt Two 64-bit words of plaintext.
 * \param Ct Two 64-bit words of ciphertext.
 * \param rk 68 64-bit words of round keys.
 */
void Simon_128_128_Encrypt(uint64_t Pt[], uint64_t Ct[], uint64_t rk[])
{
    Ct[0] = Pt[0];
    Ct[1] = Pt[1];

    for (int i = 0; i < 68; i += 2)
    {
        R64x2(Ct[1], Ct[0], rk[i], rk[i + 1]);
    }
}


/**
 * \param Pt Two 64-bit words of plaintext.
 * \param Ct Two 64-bit words of ciphertext.
 * \param rk 68 64-bit words of round keys.
 */
void Simon_128_128_Decrypt(uint64_t Pt[], uint64_t Ct[], uint64_t rk[])
{
    Pt[0] = Ct[0];
    Pt[1] = Ct[1];

    for (int i = 67; i >= 0; i -= 2)
    {
        R64x2(Pt[0], Pt[1], rk[i], rk[i - 1]);
    }
}


void Simon_128_128_EncryptOnce(uint64_t Pt[], uint64_t Ct[], uint64_t K[])
{
    uint64_t rk[68];

    Simon_128_128_KeySchedule(K, rk);
    Simon_128_128_Encrypt(Pt, Ct, rk);
}



void hash_init(uint64_t h[])
{
    // TODO: Choose better initial values.
    h[0] = UINT64_C(0xFFFFFFFFFFFFFFFF);
    h[1] = UINT64_C(0xFFFFFFFFFFFFFFFF);
}


// Miyaguchi-Preneel construction
// H_i = H_(i-1) ^ x_i ^ E_(H_(i-1))(x_i)
void hash_block(uint64_t h[], uint64_t block[])
{
    uint64_t T[2];

    Simon_128_128_EncryptOnce(block, T, h);

    h[0] ^= block[0] ^ T[0];
    h[1] ^= block[1] ^ T[1];
}


void hash_binary(uint64_t h[], const char *s, size_t len)
{
    uint8_t block[16]; // 128 / 8 == 16
    uint64_t B[2];

    hash_init(h);

    int j = 0;

    for (size_t i = 0; ; )
    {
        if (i >= len)
        {
            if (j <= 8)
            {
                for (; j < 8; j++)
                {
                    block[j] = 0;
                }

                BytesToWords64(block, B, 16);
                B[1] = len;
                hash_block(h, B);
            }
            else
            {
                for (; j < 16; j++)
                {
                    block[j] = 0;
                }
                
                BytesToWords64(block, B, 16);
                hash_block(h, B);

                B[0] = 0;
                B[1] = len;
                hash_block(h, B);
            }

            return;
        }

        block[j] = s[i];

        j++;

        if (j >= 16)
        {
            BytesToWords64(block, B, 16);
            hash_block(h, B);

            j = 0;
        }

        i++;
    }
}


int main()
{
    uint64_t h[2];

    hash_binary(h, "0123456789ABCDEF", 0);

    std::cout << std::hex << std::setfill('0') << std::setw(16) << h[1]
              << std::hex << std::setfill('0') << std::setw(16) << h[0]
              << std::endl;

    std::cout << std::hex << std::setfill('0') << std::setw(16) << (_rotl64(UINT64_C(0x1234567890ABCDEF), 8)) << std::endl;
}
